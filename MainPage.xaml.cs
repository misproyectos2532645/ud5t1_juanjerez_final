﻿namespace UD5T1
{
    //metodo asosiado al XML MainPage
    public partial class MainPage : ContentPage
    {
        //propiedad de cuenta
        decimal cuenta=0;

        //propiedad de propina
        int propina =0;

        //propiedad de personas
        int personas = 1;

        //inicializa la pagina XML
        public MainPage()
        {
            InitializeComponent();
        }

        //metodo encargado de calcular el total de la cuenta y dividirlo por las personas del grupo
        private void CalcularTotal() {
            //propina total
            var propinaTotal = cuenta * propina / 100;

            //propina por persona
            var propinaPorPersona = propinaTotal / personas;
            lblPropinaPorPersona.Text = $"{propinaPorPersona:C}";

            //subtotal
            var subTotal = cuenta / personas;
            lblSubtotal.Text = $"{subTotal:C}";

            //total
            var totalPorPersona = (cuenta+propinaTotal)/personas;
            lblTotal.Text = $"{totalPorPersona:C}";
        }

        //metodo que recupera el total de la cuenta
        private void TxtCuenta_Completed(object sender, EventArgs e) {
            cuenta = Decimal.Parse(txtCuenta.Text);
            CalcularTotal();
        }

        //metodo que recupera y actualiza el porcentaje de propina que le daremos al camarero
        private void SldPropina_ValueChanged(object sender, ValueChangedEventArgs e) {
            propina = ((int)sldPropina.Value);
            lblPropina.Text = "Propina: "+ propina.ToString() + "%";
            CalcularTotal();
        }

        //actualiza el porcentaje de la cuenta a al que tenga el botón correspondiente
        private void Button_Clicked(object sender, EventArgs e) {
            if (sender is Button){
                var btn = (Button)sender;
                var porcentaje = int.Parse(btn.Text.Replace("%", string.Empty));
                sldPropina.Value = porcentaje;
            }
        }

        //disminuye el número de personas
        private void BtnMenos_Cliked(object sender,EventArgs e) {
            if (personas>1){
                personas--;
                lblPersonas.Text = personas.ToString();
                CalcularTotal();
            }
        }

        //aumenta el número de personas
        private void BtnMas_Cliked(object sender, EventArgs e)
        {
            personas++;
            lblPersonas.Text = personas.ToString();
            CalcularTotal();
        }
    }

}
