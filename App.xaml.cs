﻿namespace UD5T1
{
    public partial class App : Application
    {
        //metodo que inicializa la App
        public App()
        {
            InitializeComponent();

            //página de inicio de la aplicación
            MainPage = new MainPage();
        }
    }
}
